from scipy.optimize import curve_fit
import numpy as np
import subprocess
import os.path
import math
import cmath
from math import log10, floor
import matplotlib
matplotlib.use('PDF')
import matplotlib.mlab as ml
import mpmath as mp
import pylab as pl
from scipy import interpolate, signal
#from matplotlib.mlab import griddata
import matplotlib.font_manager as fm
from matplotlib.ticker import MultipleLocator
import matplotlib.patches as mpatches
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
import scipy.interpolate
from scipy.interpolate import interp1d
from scipy.interpolate import RegularGridInterpolator, LinearNDInterpolator, NearestNDInterpolator, griddata
import pickle
import random
import subprocess
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf
from tensorflow import keras
from sklearn.model_selection import train_test_split
import pandas as pd
from sklearn.compose import make_column_transformer
from sklearn.preprocessing import MinMaxScaler, OneHotEncoder
from sklearn.preprocessing import StandardScaler
from keras.models import Sequential
from keras.layers import Dense
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
############################
# LOCATIONS AND PARAMETERS #
############################

# print('Triple Higgs XSEC Fitting - Read the Pickle Files')

# run number
RunNum = '14'

# Pickle File Location:
PickleLocation = 'pickles/'

# the number of runs per m2,w2,m3,w3 combo:
Nruns = 60

# Nepochs for the nn fit
Nepochs = 10000

# test the fit using paper points?
test_fit = False

# do the single example?
do_example = False

##############################
# FUNCTIONS
##############################

def func_t_hhh_CX(Lambdas, coeffs, k1, k2, k3):
    A1, A2, B1, B2, C1, C2, D1, D2, F1, F2, G1, G2, H1, H2, Q1, Q2, J1, J2, K1, K2, L1, L2, M1, M2, N1, N2, O1, O2, P1, P2 = coeffs
    Lam111, Lam112, Lam113, Lam122, Lam123, Lam1111, Lam1112, Lam1113, Lam133 = [float(lam) for lam in Lambdas]
    #M = complex(A1,A2)*k1**3 
    M = complex(A1,A2)*k1**3 + k1*(complex(B1,B2)*Lam1111 + complex(C1,C2)*Lam112**2 + complex(D1,D2)*Lam113**2) + k3*(complex(J1,J2)*Lam111*Lam113 +  complex(K1,K2)*Lam112*Lam123 + complex(L1,L2)*Lam113*Lam133 + Lam1113*complex(M1,M2)) + k1*(k1*Lam111*complex(N1,N2) + k2*Lam112*complex(O1,O2) + k3*Lam113*complex(P1,P2)) + k2*(complex(F1,F2)*Lam111*Lam112 +complex(G1,G2)*Lam112*Lam122 + complex(H1,H2)*Lam113*Lam123 + Lam1112*complex(Q1,Q2))
    return abs(M)**2

# read in data from a pickle file
def read_datfile_interp(inputfile):
    inputstream = open(inputfile, 'br')
    histogram = pickle.load(inputstream)
    return histogram


def read_interpcoeffs_hhh(outfiletag):
    out_A1i = outfiletag + '_A1i.dat'
    out_A2i = outfiletag + '_A2i.dat'
    out_B1i = outfiletag + '_B1i.dat'
    out_B2i = outfiletag + '_B2i.dat'
    out_C1i = outfiletag + '_C1i.dat'
    out_C2i = outfiletag + '_C2i.dat'
    out_D1i = outfiletag + '_D1i.dat'
    out_D2i = outfiletag + '_D2i.dat'
    out_F1i = outfiletag + '_F1i.dat'
    out_F2i = outfiletag + '_F2i.dat'
    out_G1i = outfiletag + '_G1i.dat'
    out_G2i = outfiletag + '_G2i.dat'
    out_H1i = outfiletag + '_H1i.dat'
    out_H2i = outfiletag + '_H2i.dat'
    out_Q1i = outfiletag + '_Q1i.dat'
    out_Q2i = outfiletag + '_Q2i.dat'
    out_J1i = outfiletag + '_J1i.dat'
    out_J2i = outfiletag + '_J2i.dat'
    out_K1i = outfiletag + '_K1i.dat'
    out_K2i = outfiletag + '_K2i.dat'
    out_L1i = outfiletag + '_L1i.dat'
    out_L2i = outfiletag + '_L2i.dat'
    out_M1i = outfiletag + '_M1i.dat'
    out_M2i = outfiletag + '_M2i.dat'
    out_N1i = outfiletag + '_N1i.dat'
    out_N2i = outfiletag + '_N2i.dat'
    out_O1i = outfiletag + '_O1i.dat'
    out_O2i = outfiletag + '_O2i.dat'
    out_P1i = outfiletag + '_P1i.dat'
    out_P2i = outfiletag + '_P2i.dat'
    A1i = read_datfile_interp(out_A1i)
    A2i = read_datfile_interp(out_A2i)
    B1i = read_datfile_interp(out_B1i)
    B2i = read_datfile_interp(out_B2i)
    C1i = read_datfile_interp(out_C1i)
    C2i = read_datfile_interp(out_C2i)
    D1i = read_datfile_interp(out_D1i)
    D2i = read_datfile_interp(out_D2i)
    F1i = read_datfile_interp(out_F1i)
    F2i = read_datfile_interp(out_F2i)
    G1i = read_datfile_interp(out_G1i)
    G2i = read_datfile_interp(out_G2i)
    H1i = read_datfile_interp(out_H1i)
    H2i = read_datfile_interp(out_H2i)
    Q1i = read_datfile_interp(out_Q1i)
    Q2i = read_datfile_interp(out_Q2i)
    J1i = read_datfile_interp(out_J1i)
    J2i = read_datfile_interp(out_J2i)
    K1i = read_datfile_interp(out_K1i)
    K2i = read_datfile_interp(out_K2i)
    L1i = read_datfile_interp(out_L1i)
    L2i = read_datfile_interp(out_L2i)
    M1i = read_datfile_interp(out_M1i)
    M2i = read_datfile_interp(out_M2i)
    N1i = read_datfile_interp(out_N1i)
    N2i = read_datfile_interp(out_N2i)
    O1i = read_datfile_interp(out_O1i)
    O2i = read_datfile_interp(out_O2i)
    P1i = read_datfile_interp(out_P1i)
    P2i = read_datfile_interp(out_P2i)
    return A1i, A2i, B1i, B2i, C1i, C2i, D1i, D2i, F1i, F2i, G1i, G2i, H1i, H2i, Q1i, Q2i,  J1i, J2i, K1i, K2i, L1i, L2i, M1i, M2i, N1i, N2i, O1i, O2i, P1i, P2i


#TEST POINTS FROM PAPER:
def do_Test_XSECS_Paper_HardWired(A1i, A2i, B1i, B2i, C1i, C2i, D1i, D2i, F1i, F2i, G1i, G2i, H1i, H2i, Q1i, Q2i,  J1i, J2i, K1i, K2i, L1i, L2i, M1i, M2i, N1i, N2i, O1i, O2i, P1i, P2i):
    k1 = 0.966
    k2 = 0.094
    k3 = 0.239
    LambdasTestArray = [[ 27.940871698413677 , 39.07032671865848 , 160.76112043143365 , -462.90567518636254 , -65.75926975945164 , 0.07061899381530284 , -0.19910092033686816 , -0.017181659533562252 , 160.76112043143365 ], [ 27.940871698413677 , 40.75125909709511 , 134.28360858906584 , -403.64424557437263 , -69.49998457931159 , 0.06350041346960428 , -0.1639973796079403 , -0.01725184386475773 , 134.28360858906584 ], [ 27.940871698413677 , 46.10558037976005 , 159.62744049640688 , -484.1112727500433 , -81.41538892828633 , 0.07172645899277884 , -0.20388353227857536 , -0.026815537852029992 , 159.62744049640688 ], [ 27.940871698413677 , 46.80772660250952 , 133.7714241651397 , -423.03798996766324 , -82.97793172586779 , 0.06458445962260628 , -0.1687560454873902 , -0.02558489260209645 , 133.7714241651397 ], [ 27.940871698413677 , 54.2307629262041 , 160.19371700470802 , -513.2060386220488 , -99.49701502785551 , 0.07353390637443129 , -0.21200120316375362 , -0.03810165077376178 , 160.19371700470802 ], [ 27.940871698413677 , 40.9650263180015 , 160.76112043143365 , -469.3661073687232 , -69.97569807112292 , 0.07100326769367478 , -0.20081123184035396 , -0.01980220722517033 , 160.76112043143365 ], [ 27.940871698413677 , 44.495633321358746 , 134.28360858906584 , -416.4115865025188 , -77.83264348426506 , 0.06425982947715113 , -0.16737735937430243 , -0.022430665474955622 , 134.28360858906584 ], [ 27.940871698413677 , 49.20094596370066 , 144.76394993672903 , -458.1810260031162 , -88.30375639700044 , 0.06816676084212529 , -0.18612005857468847 , -0.029831012960261746 , 144.76394993672903 ], [ 27.940871698413677 , 51.67529140493219 , 158.498268235078 , -500.3308163600932 , -93.81011749766301 , 0.07253795979242776 , -0.20734945958275983 , -0.03442281676277198 , 158.498268235078 ], [ 27.940871698413677 , 44.495633321358746 , 158.498268235078 , -475.8500521494782 , -77.83264348426506 , 0.07108181600155146 , -0.20086851040448991 , -0.024492674578772102 , 158.498268235078 ]]
    m2TestArray = [255, 263, 287, 290, 320, 264, 280, 300, 310, 280]
    m3TestArray = [504, 455, 502, 454, 503, 504, 455, 475, 500, 500]
    w2TestArray = [0.086, 0.12, 0.21, 0.22, 0.32, 0.13, 0.18, 0.25, 0.29, 0.18]
    w3TestArray = [11, 7.6, 11, 7, 10, 11, 7.4, 8.4, 10, 10.6]
    XSECS_PAPER = [0.024874916179600002, 0.04197644948870001, 0.029847062189100004, 0.040125948665, 0.02959640498, 0.028373961964999996, 0.04228367336530001, 0.035458564898400005, 0.030135293571900004, 0.03242979478]

    k1t = 0.966
    k2t = 0.094
    k3t = 0.239
    TESTNAME = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']
    cc = 0
    for LambdasTest in LambdasTestArray:
        PT = [m2TestArray[cc], w2TestArray[cc], m3TestArray[cc], w3TestArray[cc]]
        poptTest = A1i(PT), A2i(PT), B1i(PT), B2i(PT), C1i(PT), C2i(PT), D1i(PT), D2i(PT), F1i(PT), F2i(PT), G1i(PT), G2i(PT), H1i(PT), H2i(PT), Q1i(PT), Q2i(PT),  J1i(PT), J2i(PT), K1i(PT), K2i(PT), L1i(PT), L2i(PT), M1i(PT), M2i(PT), N1i(PT), N2i(PT), O1i(PT), O2i(PT), P1i(PT), P2i(PT)
        XSEC_FIT = func_t_hhh_CX(LambdasTest, poptTest, k1t, k2t, k3t)
        print("TEST POINT", TESTNAME[cc], "FIT=", XSEC_FIT, "MG5=", XSECS_PAPER[cc], 'RELATIVE ERROR=', (XSECS_PAPER[cc] - XSEC_FIT)/XSECS_PAPER[cc])
        cc = cc + 1

# get the cross section for a point:
def get_hhh_xsec(k1, k2, k3, m2, w2, m3, w3, Lambdas, InterpCoeffs):
    A1i, A2i, B1i, B2i, C1i, C2i, D1i, D2i, F1i, F2i, G1i, G2i, H1i, H2i, Q1i, Q2i,  J1i, J2i, K1i, K2i, L1i, L2i, M1i, M2i, N1i, N2i, O1i, O2i, P1i, P2i = InterpCoeffs
    # create array and calculated coefficients at the point in question:
    PT = [m2, w2, m3, w3]
    # get the coefficients at that point:
    poptTest = A1i(PT), A2i(PT), B1i(PT), B2i(PT), C1i(PT), C2i(PT), D1i(PT), D2i(PT), F1i(PT), F2i(PT), G1i(PT), G2i(PT), H1i(PT), H2i(PT), Q1i(PT), Q2i(PT),  J1i(PT), J2i(PT), K1i(PT), K2i(PT), L1i(PT), L2i(PT), M1i(PT), M2i(PT), N1i(PT), N2i(PT), O1i(PT), O2i(PT), P1i(PT), P2i(PT)
    # get the cross section:
    XSEC_FIT = func_t_hhh_CX(Lambdas, poptTest, k1, k2, k3)
    return XSEC_FIT


#TEST POINTS FROM PAPER:
def do_Test_XSECS_Paper_HardWired_nn(ModelCoeffs, PredictorScalers, TargetScalers):
    k1 = 0.966
    k2 = 0.094
    k3 = 0.239
    LambdasTestArray = [[ 27.940871698413677 , 39.07032671865848 , 160.76112043143365 , -462.90567518636254 , -65.75926975945164 , 0.07061899381530284 , -0.19910092033686816 , -0.017181659533562252 , 160.76112043143365 ], [ 27.940871698413677 , 40.75125909709511 , 134.28360858906584 , -403.64424557437263 , -69.49998457931159 , 0.06350041346960428 , -0.1639973796079403 , -0.01725184386475773 , 134.28360858906584 ], [ 27.940871698413677 , 46.10558037976005 , 159.62744049640688 , -484.1112727500433 , -81.41538892828633 , 0.07172645899277884 , -0.20388353227857536 , -0.026815537852029992 , 159.62744049640688 ], [ 27.940871698413677 , 46.80772660250952 , 133.7714241651397 , -423.03798996766324 , -82.97793172586779 , 0.06458445962260628 , -0.1687560454873902 , -0.02558489260209645 , 133.7714241651397 ], [ 27.940871698413677 , 54.2307629262041 , 160.19371700470802 , -513.2060386220488 , -99.49701502785551 , 0.07353390637443129 , -0.21200120316375362 , -0.03810165077376178 , 160.19371700470802 ], [ 27.940871698413677 , 40.9650263180015 , 160.76112043143365 , -469.3661073687232 , -69.97569807112292 , 0.07100326769367478 , -0.20081123184035396 , -0.01980220722517033 , 160.76112043143365 ], [ 27.940871698413677 , 44.495633321358746 , 134.28360858906584 , -416.4115865025188 , -77.83264348426506 , 0.06425982947715113 , -0.16737735937430243 , -0.022430665474955622 , 134.28360858906584 ], [ 27.940871698413677 , 49.20094596370066 , 144.76394993672903 , -458.1810260031162 , -88.30375639700044 , 0.06816676084212529 , -0.18612005857468847 , -0.029831012960261746 , 144.76394993672903 ], [ 27.940871698413677 , 51.67529140493219 , 158.498268235078 , -500.3308163600932 , -93.81011749766301 , 0.07253795979242776 , -0.20734945958275983 , -0.03442281676277198 , 158.498268235078 ], [ 27.940871698413677 , 44.495633321358746 , 158.498268235078 , -475.8500521494782 , -77.83264348426506 , 0.07108181600155146 , -0.20086851040448991 , -0.024492674578772102 , 158.498268235078 ]]
    m2TestArray = [255, 263, 287, 290, 320, 264, 280, 300, 310, 280]
    m3TestArray = [504, 455, 502, 454, 503, 504, 455, 475, 500, 500]
    w2TestArray = [0.086, 0.12, 0.21, 0.22, 0.32, 0.13, 0.18, 0.25, 0.29, 0.18]
    w3TestArray = [11, 7.6, 11, 7, 10, 11, 7.4, 8.4, 10, 10.6]
    XSECS_PAPER = [0.024874916179600002, 0.04197644948870001, 0.029847062189100004, 0.040125948665, 0.02959640498, 0.028373961964999996, 0.04228367336530001, 0.035458564898400005, 0.030135293571900004, 0.03242979478]

    k1t = 0.966
    k2t = 0.094
    k3t = 0.239
    TESTNAME = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']
    cc = 0
    for LambdasTest in LambdasTestArray:
        PT = [m2TestArray[cc], w2TestArray[cc], m3TestArray[cc], w3TestArray[cc]]
        #XSEC_FIT = func_t_hhh_CX(LambdasTest, poptTest, k1t, k2t, k3t)
        XSEC_FIT = get_hhh_xsec_nn(k1t, k2t, k3t, PT[0], PT[1], PT[2], PT[3], LambdasTest, ModelCoeffs, PredictorScalers, TargetScalers)
        print("TEST POINT", TESTNAME[cc], "FIT=", XSEC_FIT, "MG5=", XSECS_PAPER[cc], 'RELATIVE ERROR=', (XSECS_PAPER[cc] - XSEC_FIT)/XSECS_PAPER[cc])
        cc = cc + 1

# write the neural network output
def write_nn(ModelCoeffs, PredictorScalers, TargetScalers, outfiletag):
    out_ModelCoeffs = outfiletag + '_ModelCoeffs.dat'
    out_PredictorScalers = outfiletag + '_PredictorScalers.dat'
    out_TargetScalers = outfiletag + '_TargetScalers.dat'
    write_datfile_interp(out_ModelCoeffs, ModelCoeffs)
    write_datfile_interp(out_PredictorScalers, PredictorScalers)
    write_datfile_interp(out_TargetScalers, TargetScalers)
    
# read the neural network output
def read_nn(outfiletag):
    out_ModelCoeffs = outfiletag + '_ModelCoeffs.dat'
    out_PredictorScalers = outfiletag + '_PredictorScalers.dat'
    out_TargetScalers = outfiletag + '_TargetScalers.dat'
    ModelCoeffs = read_datfile_interp(out_ModelCoeffs)
    PredictorScalers = read_datfile_interp(out_PredictorScalers)
    TargetScalers = read_datfile_interp(out_TargetScalers)
    return ModelCoeffs, PredictorScalers, TargetScalers

# the xsec from nn
def get_hhh_xsec_nn(k1, k2, k3, m2, w2, m3, w3, Lambdas, ModelCoeffs, Predictors, Targets):
    A1nn, A2nn, B1nn, B2nn, C1nn, C2nn, D1nn, D2nn, F1nn, F2nn, G1nn, G2nn, H1nn, H2nn, Q1nn, Q2nn,  J1nn, J2nn, K1nn, K2nn, L1nn, L2nn, M1nn, M2nn, N1nn, N2nn, O1nn, O2nn, P1nn, P2nn = ModelCoeffs
    PredictorScalerFit_A1nn, PredictorScalerFit_A2nn, PredictorScalerFit_B1nn, PredictorScalerFit_B2nn, PredictorScalerFit_C1nn, PredictorScalerFit_C2nn, PredictorScalerFit_D1nn, PredictorScalerFit_D2nn, PredictorScalerFit_F1nn, PredictorScalerFit_F2nn, PredictorScalerFit_G1nn, PredictorScalerFit_G2nn, PredictorScalerFit_H1nn, PredictorScalerFit_H2nn, PredictorScalerFit_Q1nn, PredictorScalerFit_Q2nn,  PredictorScalerFit_J1nn, PredictorScalerFit_J2nn, PredictorScalerFit_K1nn, PredictorScalerFit_K2nn, PredictorScalerFit_L1nn, PredictorScalerFit_L2nn, PredictorScalerFit_M1nn, PredictorScalerFit_M2nn, PredictorScalerFit_N1nn, PredictorScalerFit_N2nn, PredictorScalerFit_O1nn, PredictorScalerFit_O2nn, PredictorScalerFit_P1nn, PredictorScalerFit_P2nn = Predictors
    TargetVarScalerFit_A1nn, TargetVarScalerFit_A2nn, TargetVarScalerFit_B1nn, TargetVarScalerFit_B2nn, TargetVarScalerFit_C1nn, TargetVarScalerFit_C2nn, TargetVarScalerFit_D1nn, TargetVarScalerFit_D2nn, TargetVarScalerFit_F1nn, TargetVarScalerFit_F2nn, TargetVarScalerFit_G1nn, TargetVarScalerFit_G2nn, TargetVarScalerFit_H1nn, TargetVarScalerFit_H2nn, TargetVarScalerFit_Q1nn, TargetVarScalerFit_Q2nn,  TargetVarScalerFit_J1nn, TargetVarScalerFit_J2nn, TargetVarScalerFit_K1nn, TargetVarScalerFit_K2nn, TargetVarScalerFit_L1nn, TargetVarScalerFit_L2nn, TargetVarScalerFit_M1nn, TargetVarScalerFit_M2nn, TargetVarScalerFit_N1nn, TargetVarScalerFit_N2nn, TargetVarScalerFit_O1nn, TargetVarScalerFit_O2nn, TargetVarScalerFit_P1nn, TargetVarScalerFit_P2nn = Targets
    # create array and calculated coefficients at the point in question:
    point = [m2, w2, m3, w3]
    # get the coefficients at that point:
    #poptTest = A1i(PT), A2i(PT), B1i(PT), B2i(PT), C1i(PT), C2i(PT), D1i(PT), D2i(PT), F1i(PT), F2i(PT), G1i(PT), G2i(PT), H1i(PT), H2i(PT), Q1i(PT), Q2i(PT),  J1i(PT), J2i(PT), K1i(PT), K2i(PT), L1i(PT), L2i(PT), M1i(PT), M2i(PT), N1i(PT), N2i(PT), O1i(PT), O2i(PT), P1i(PT), P2i(PT)

    A1 = get_nnpred(point, A1nn, PredictorScalerFit_A1nn, TargetVarScalerFit_A1nn)
    A2 = get_nnpred(point, A2nn, PredictorScalerFit_A2nn, TargetVarScalerFit_A2nn)
    B1 = get_nnpred(point, B1nn, PredictorScalerFit_B1nn, TargetVarScalerFit_B1nn)
    B2 = get_nnpred(point, B2nn, PredictorScalerFit_B2nn, TargetVarScalerFit_B2nn)
    C1 = get_nnpred(point, C1nn, PredictorScalerFit_C1nn, TargetVarScalerFit_C1nn)
    C2 = get_nnpred(point, C2nn, PredictorScalerFit_C2nn, TargetVarScalerFit_C2nn)
    D1 = get_nnpred(point, D1nn, PredictorScalerFit_D1nn, TargetVarScalerFit_D1nn)
    D2 = get_nnpred(point, D2nn, PredictorScalerFit_D2nn, TargetVarScalerFit_D2nn)
    F1 = get_nnpred(point, F1nn, PredictorScalerFit_F1nn, TargetVarScalerFit_F1nn)
    F2 = get_nnpred(point, F2nn, PredictorScalerFit_F2nn, TargetVarScalerFit_F2nn)
    G1 = get_nnpred(point, G1nn, PredictorScalerFit_G1nn, TargetVarScalerFit_G1nn)
    G2 = get_nnpred(point, G2nn, PredictorScalerFit_G2nn, TargetVarScalerFit_G2nn)
    H1 = get_nnpred(point, H1nn, PredictorScalerFit_H1nn, TargetVarScalerFit_H1nn)
    H2 = get_nnpred(point, H2nn, PredictorScalerFit_H2nn, TargetVarScalerFit_H2nn)
    Q1 = get_nnpred(point, Q1nn, PredictorScalerFit_Q1nn, TargetVarScalerFit_Q1nn)
    Q2 = get_nnpred(point, Q2nn, PredictorScalerFit_Q2nn, TargetVarScalerFit_Q2nn)
    J1 = get_nnpred(point, J1nn, PredictorScalerFit_J1nn, TargetVarScalerFit_J1nn)
    J2 = get_nnpred(point, J2nn, PredictorScalerFit_J2nn, TargetVarScalerFit_J2nn)
    K1 = get_nnpred(point, K1nn, PredictorScalerFit_K1nn, TargetVarScalerFit_K1nn)
    K2 = get_nnpred(point, K2nn, PredictorScalerFit_K2nn, TargetVarScalerFit_K2nn)
    L1 = get_nnpred(point, L1nn, PredictorScalerFit_L1nn, TargetVarScalerFit_L1nn)
    L2 = get_nnpred(point, L2nn, PredictorScalerFit_L2nn, TargetVarScalerFit_L2nn)
    M1 = get_nnpred(point, M1nn, PredictorScalerFit_M1nn, TargetVarScalerFit_M1nn)
    M2 = get_nnpred(point, M2nn, PredictorScalerFit_M2nn, TargetVarScalerFit_M2nn)
    N1 = get_nnpred(point, N1nn, PredictorScalerFit_N1nn, TargetVarScalerFit_N1nn)
    N2 = get_nnpred(point, N2nn, PredictorScalerFit_N2nn, TargetVarScalerFit_N2nn)
    O1 = get_nnpred(point, O1nn, PredictorScalerFit_O1nn, TargetVarScalerFit_O1nn)
    O2 = get_nnpred(point, O2nn, PredictorScalerFit_O2nn, TargetVarScalerFit_O2nn)
    P1 = get_nnpred(point, P1nn, PredictorScalerFit_P1nn, TargetVarScalerFit_P1nn)
    P2 = get_nnpred(point, P2nn, PredictorScalerFit_P2nn, TargetVarScalerFit_P2nn)
    poptTest = A1 , A2 , B1 , B2 , C1 , C2 , D1 , D2 , F1 , F2 , G1 , G2 , H1 , H2 , Q1 , Q2 ,  J1 , J2 , K1 , K2 , L1 , L2 , M1 , M2 , N1 , N2 , O1 , O2 , P1 , P2 

    # get the cross section:
    XSEC_FIT = func_t_hhh_CX(Lambdas, poptTest, k1, k2, k3)
    return XSEC_FIT

# get the predictions for a single coefficient
def get_nnpred(point, model, PredictorScalerFit, TargetVarScalerFit):
    X=PredictorScalerFit.transform([point])
    Predictions=model.predict(X, verbose=0)
    Predictions=TargetVarScalerFit.inverse_transform(Predictions)
    return Predictions

################################################
# DO THE READING AND PRINT XSEC FOR GIVEN POINT
################################################

# filename tag:
outfiletag = PickleLocation + 'TRSM_hhh_Run' + str(RunNum) + '_N' + str(Nruns)
outfiletag_nn = PickleLocation + 'TRSM_hhh_Run' + str(RunNum) + '_N' + str(Nruns) + '_NEPOCHS' + str(Nepochs)

print('Reading interpolated coefficients and nn results')
# get the interpolated coefficients
A1i, A2i, B1i, B2i, C1i, C2i, D1i, D2i, F1i, F2i, G1i, G2i, H1i, H2i, Q1i, Q2i,  J1i, J2i, K1i, K2i, L1i, L2i, M1i, M2i, N1i, N2i, O1i, O2i, P1i, P2i = read_interpcoeffs_hhh(outfiletag)
# get the nn fit:
ModelCoeffs, PredictorScalers, TargetScalers = read_nn(outfiletag_nn)
# put them in a single variable
InterpCoeffs = A1i, A2i, B1i, B2i, C1i, C2i, D1i, D2i, F1i, F2i, G1i, G2i, H1i, H2i, Q1i, Q2i,  J1i, J2i, K1i, K2i, L1i, L2i, M1i, M2i, N1i, N2i, O1i, O2i, P1i, P2i
  
# TEST THE FIT?
if test_fit is True:
    do_Test_XSECS_Paper_HardWired(A1i, A2i, B1i, B2i, C1i, C2i, D1i, D2i, F1i, F2i, G1i, G2i, H1i, H2i, Q1i, Q2i,  J1i, J2i, K1i, K2i, L1i, L2i, M1i, M2i, N1i, N2i, O1i, O2i, P1i, P2i)
    do_Test_XSECS_Paper_HardWired_nn(ModelCoeffs, PredictorScalers, TargetScalers)


# One point example:
if do_example is True:
    # mixing coefficients:
    k1 = 0.966
    k2 = 0.094
    k3 = 0.239
    # Lambdas: [Lam111, Lam112, Lam113, Lam123, Lam122, Lam1111, Lam1112, Lam1113, Lam133]
    Lambdas = [ 27.940871698413677 , 39.07032671865848 , 160.76112043143365 , -462.90567518636254 , -65.75926975945164 , 0.07061899381530284 , -0.19910092033686816 , -0.017181659533562252 , 160.76112043143365 ]
    # masses and widths:
    m2 = 255
    m3 = 504
    w2 = 0.086
    w3 = 11
    # calculate the XSEC, given the values above and the interpolated coefficients from the fit:
    XSEC = get_hhh_xsec(k1, k2, k3, m2, w2, m3, w3, Lambdas, InterpCoeffs)
    # print the example:
    #print('EXAMPLE XSEC =', XSEC)
